$(document).ready(function() {

    $('.modal-body').on('submit', 'form', function (event) {
        let id = $(this).data('id');
        let url;
        let action = $(this).data('action');
        switch (action) {
            case 'edit':
                let edit = $('td:last-child .edit', '#' + id);
                url = edit.data('target');
                break;
            case 'details':
                let details = $('td:last-child .details', '#' + id);
                url = details.data('target');
                break;
            case 'create':
                let n_new = $('#new');
                url = n_new.data('target');
        }
        $.ajax(
            {
                url: url,
                data: $('form').serialize(),
                method: 'POST',
                success: function (data) {
                    if(data.includes( "form" ))
                    {
                        //$('.modal-body form').replaceWith(data)
                        $('.modal-body').html(data);
                    }else {
                        if(action === 'edit')
                        {
                            let elm = $('#' + id);
                            let num = $('th', elm).text();
                            elm.replaceWith(data);
                            $('th', '#' + id).text(num);
                        }else if(action === 'create')
                        {
                            $('tbody').append(data);
                            hiddeNo();
                        }else if(action === 'delete')
                        {
                            let elm = $('tbody tr th');
                            let num = elm.text() - 1;
                            elm.text(num)
                        }
                        $('.modal').modal('hide');
                    }
                },
            });
        event.preventDefault();
    });
    //
    $('tfoot').on('click', '#new', function (event) {

        $.ajax({
            url: $(this).data('target'),
            //data: $(this).serialize(),
            method: 'GET',
            success: function(data) {
                $('.modal-body').html(data);
                $('.modal').modal('show');
                //handleForm(url);
            },
        });
        event.preventDefault();
    });
    function hiddeNo(){
        const no = $('#no');
        if ($('tbody tr').length > 0 ) {
            no.addClass('d-none');
        }else{
            no.removeClass('d-none')
        }
    }
    $('tbody').on('click', '.action',function (event) {
        let action = $(this).data('action');
        let task_id = $(this).data('id');
        $.ajax({
            url: $(this).data('target'),
            method: 'GET',
            success: function (data) {
                if (action === 'delete'){
                    if( data['res'] === true){
                        $(`#${task_id}`).addClass('d-none');
                        hiddeNo();
                    }
                }else if(action === 'edit'){
                    $('.modal-body').html(data);
                    $('.modal').modal('show');
                    $('.modal-title').text("Edit your task");
                }else if(action === 'details'){
                    $('.modal-body').html(data);
                    $('.modal').modal('show');
                    $('.modal-title').text("Your task" + $('#' + task_id).find('td:first-child').text());
                }else if(action === 'done'){
                    let elm = $('#' + task_id);
                    let num = $('th', elm).text();
                    elm.replaceWith(data);
                    $('th', '#' + task_id).text(num);
                }
            },
        });
    });
});